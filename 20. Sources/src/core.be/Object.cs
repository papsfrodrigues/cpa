﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class Object
    {
        public core.be.RsAccount account;
        public core.be.RsItem item;
        public core.be.RsLocation location;

        public Object()
        {
            account = new RsAccount();
            item = new RsItem();
            location = new RsLocation();
        }
    }
}
