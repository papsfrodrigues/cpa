﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsAccount
    {
        public int accountId;
        public int level1AccountId;
        public int level2AccountId;
        public int level3AccountId;
        public int level2ParentAccountId;
        public int level3ParentAccountId;
        public string accountCode;
        public string accountName;
        public DateTime leaseExpireDtime;
        public DateTime setupDtime;
        public int isDeletable;
        public int isOpen;
        public int forceAuthorization;
        public string salesmanCode;
        public string poNumber;
        public int invoiceCycleId;
        public int isDelinquent;
        public int applyPriorityToWo;
        public string accountingCustCode;
        public int tallyInOutDaysContainer;
        public DateTime tallyInOutDaysContainerDate;
        public int tallyInOutDaysFilefolder;
        public DateTime tallyInOutDaysFilefolderDate;
        public int tallyInOutDaysTape;
        public DateTime tallyInOutDaysTapeDate;
        public int updtUserId;
        public DateTime updtDtime;
        public int contentValidation;
        public int useParentRetention;
        public int applyPriorityToItem;
        public int applyLinkedPrioritiesToActions;
        public int defaultPriorityId;
        public int priorityRequired;
        public string accountGroup;
        public int enableConector;
        public int woChargeId;
        public int woPickupReceiveChargeId;
        public string accountReference;
        public int contactPinRequiredByDefault;

        public RsAccount()
        {
            accountId = 0;
            level1AccountId = 0;
            level2AccountId = 0;
            level3AccountId = 0;
            level2ParentAccountId = 0;
            level3ParentAccountId = 0;
            accountCode = "";
            accountName = "";
            leaseExpireDtime = new DateTime(1753, 1, 1);
            setupDtime = new DateTime(1753, 1, 1);
            isDeletable = 0;
            isOpen = 0;
            forceAuthorization = 0;
            salesmanCode = "";
            poNumber = "";
            invoiceCycleId = 0;
            isDelinquent = 0;
            applyPriorityToWo = 0;
            accountingCustCode = "";
            tallyInOutDaysContainer = 0;
            tallyInOutDaysContainerDate = new DateTime(1753, 1, 1);
            tallyInOutDaysFilefolder = 0;
            tallyInOutDaysFilefolderDate = new DateTime(1753, 1, 1);
            tallyInOutDaysTape = 0;
            tallyInOutDaysTapeDate = new DateTime(1753, 1, 1);
            updtUserId = 0;
            updtDtime = new DateTime(1753, 1, 1);
            contentValidation = 0;
            useParentRetention = 0;
            applyPriorityToItem = 0;
            applyLinkedPrioritiesToActions = 0;
            defaultPriorityId = 0;
            priorityRequired = 0;
            accountGroup = "";
            enableConector = 0;
            woChargeId = 0;
            woPickupReceiveChargeId = 0;
            accountReference = "";
            contactPinRequiredByDefault = 0;
        }
    }
}
