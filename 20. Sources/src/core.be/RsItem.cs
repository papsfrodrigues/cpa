﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsItem
    {
        public int itemId;
        public int locationId;
        public int containerItemId;
        public DateTime containedInDtime;
        public int accountId;
        public Int16 baseObjectId;
        public Int16 objectId;
        public Int16 itemSecurityId;
        public int categoryId;
        public Int16 containeeObjectID;
        public Byte itemStatusID;
        public string itemCode;
        public string alternateCode;
        public DateTime statusDtime;
        public DateTime addDtime;
        public DateTime destroyDtime;
        public Byte permanentFlag;
        public Byte chargeForStorage;
        public Byte containeeDefaultCharge;
        public DateTime fromDtime;
        public DateTime toDtime;
        public string seqBegin;
        public string seqEnd;
        public string userField1;
        public string userField2;
        public string userField3;
        public string userField4;
        public string itemDesc;
        public DateTime userDtime;
        public string itemSetName;
        public Int16 accessCount;
        public int updtUserID;
        public DateTime updtDtime;
        public int recordSeriesID;
        public DateTime pendingAddDtime;

        public RsItem()
        {
            this.itemId = 0;
            this.locationId = 0;
            this.containerItemId = 0;
            this.containedInDtime = new DateTime(1753, 1, 1);
            this.accountId = 0;
            this.baseObjectId = 0;
            this.objectId = 0;
            this.itemSecurityId = 0;
            this.categoryId = 0;
            this.containeeObjectID = 0;
            this.itemStatusID = 0;
            this.itemCode = "";
            this.alternateCode = "";
            this.statusDtime = new DateTime(1753, 1, 1);
            this.addDtime = new DateTime(1753, 1, 1);
            this.destroyDtime = new DateTime(1753, 1, 1);
            this.permanentFlag = 0;
            this.chargeForStorage = 0;
            this.containeeDefaultCharge = 0;
            this.fromDtime = new DateTime(1753, 1, 1);
            this.toDtime = new DateTime(1753, 1, 1);
            this.seqBegin = "";
            this.seqEnd = "";
            this.userField1 = "";
            this.userField2 = "";
            this.userField3 = "";
            this.userField4 = "";
            this.itemDesc = "";
            this.userDtime = new DateTime(1753, 1, 1);
            this.itemSetName = "";
            this.accessCount = 0;
            this.updtUserID = 0;
            this.updtDtime = new DateTime(1753, 1, 1);
            this.recordSeriesID = 0;
            this.pendingAddDtime = new DateTime(1753, 1, 1);
        }
    }
}
