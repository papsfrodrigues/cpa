﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsItemContent
    {
        public int itemContentId;
        public int itemId;
        public int sequence;
        public string itemContent;
    }
}
