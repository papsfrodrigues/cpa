﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsItemDescription
    {
        public int itemDescId;
        public int itemId;
        public Int16 sequence;
        public string itemDesc;

        public RsItemDescription()
        {
            this.itemDescId = 0;
            this.itemId = 0;
            this.sequence = 0;
            this.itemDesc = "";
        }
    }
}
