﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsLocation
    {
        public int locationId;
        public int objectId;
        public int capacity;
        public int currentQuantity;
        public int accountId;
        public string building;
        public string floor;
        public string room;
        public int holdAreaId;
        public string locationCode;
        public string locationDesc;
        public int updtUserId;
        public DateTime updtDtime;
    }
}
