﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.be
{
    public class RsObject
    {
        public int objectId;
        public int parentObjectId;
        public string objectCode;
        public string objectDesc;
        public int enableChildren;
        public int isDeletable;
        public int objectTypeId;
        public int isPublic;
        public int updtUserId;
        public DateTime updtDtime;
        public Decimal unitVolume;
    }
}
