﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class Object
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public Object(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.Object data)
        {
            core.bs.RsAccount bsAccount = new RsAccount(dataProviderFactory, dataConnection);
            core.bs.RsItem bsItem = new RsItem(dataProviderFactory, dataConnection);
            core.bs.RsLocation bsLocation = new RsLocation(dataProviderFactory, dataConnection);

            core.be.RsAccount beAccount = new be.RsAccount();
            core.be.RsItem beItem = new be.RsItem();
            core.be.RsLocation beLocation = new be.RsLocation();

            bsAccount.FillData(dr, beAccount);
            bsItem.FillData(dr, beItem);
            bsLocation.FillData(dr, beLocation);
        }
    }
}
