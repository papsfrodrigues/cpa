﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsAccount
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsAccount(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsAccount data)
        {
            data.accountId = Convert.ToInt32(dr["AccountID"]);

            if (!dr.IsNull("Level1AccountID"))
                data.level1AccountId = Convert.ToInt32(dr["Level1AccountID"]);
            if (!dr.IsNull("Level2AccountID"))
                data.level2AccountId = Convert.ToInt32(dr["Level2AccountID"]);
            if (!dr.IsNull("Level3AccountID"))
                data.level3AccountId = Convert.ToInt32(dr["Level3AccountID"]);
            if (!dr.IsNull("Level2ParentAccountID"))
                data.level2ParentAccountId = Convert.ToInt32(dr["Level2ParentAccountID"]);
            if (!dr.IsNull("Level3ParentAccountID"))
                data.level3ParentAccountId = Convert.ToInt32(dr["Level3ParentAccountID"]);

            data.accountCode = dr["AccountCode"].ToString();

            if (!dr.IsNull("AccountName"))
                data.accountName = dr["AccountName"].ToString();
            if (!dr.IsNull("LeaseExpireDtime"))
                data.leaseExpireDtime = Convert.ToDateTime(dr["LeaseExpireDtime"]);

            data.setupDtime = Convert.ToDateTime(dr["SetupDtime"]);
            data.isDeletable = Convert.ToInt32(dr["IsDeletable"]);
            data.isOpen = Convert.ToInt32(dr["IsOpen"]);
            data.forceAuthorization = Convert.ToInt32(dr["ForceAuthorization"]);

            if (!dr.IsNull("SalesmanCode"))
                data.salesmanCode = dr["SalesmanCode"].ToString();
            if (!dr.IsNull("PONumber"))
                data.poNumber = dr["PONumber"].ToString();
            if (!dr.IsNull("InvoiceCycleID"))
                data.invoiceCycleId = Convert.ToInt32(dr["InvoiceCycleID"]);

            data.isDelinquent = Convert.ToInt32(dr["IsDelinquent"]);
            data.applyPriorityToWo = Convert.ToInt32(dr["ApplyPriorityToWO"]);

            if (!dr.IsNull("AccountingCustCode"))
                data.accountingCustCode = dr["AccountingCustCode"].ToString();

            data.tallyInOutDaysContainer = Convert.ToInt32(dr["TallyInOutDaysContainer"]);

            if (!dr.IsNull("TallyInOutDaysContainerDate"))
                data.tallyInOutDaysContainerDate = Convert.ToDateTime(dr["TallyInOutDaysContainerDate"]);

            data.tallyInOutDaysFilefolder = Convert.ToInt32(dr["TallyInOutDaysFilefolder"]);

            if (!dr.IsNull("TallyInOutDaysFilefolderDate"))
                data.tallyInOutDaysFilefolderDate = Convert.ToDateTime(dr["TallyInOutDaysFilefolderDate"]);

            data.tallyInOutDaysTape = Convert.ToInt32(dr["TallyInOutDaysTape"]);

            if (!dr.IsNull("TallyInOutDaysTapeDate"))
                data.tallyInOutDaysTapeDate = Convert.ToDateTime(dr["TallyInOutDaysTapeDate"]);

            data.updtUserId = Convert.ToInt32(dr["UpdtUserID"]);
            data.updtDtime = Convert.ToDateTime(dr["UpdtDtime"]);
            data.contentValidation = Convert.ToInt32(dr["ContentValidation"]);
            data.useParentRetention = Convert.ToInt32(dr["UseParentRetention"]);
            data.applyPriorityToItem = Convert.ToInt32(dr["ApplyPriorityToItem"]);
            data.applyLinkedPrioritiesToActions = Convert.ToInt32(dr["ApplyLinkedPrioritiesToActions"]);

            if (!dr.IsNull("DefaultPriorityID"))
                data.defaultPriorityId = Convert.ToInt32(dr["DefaultPriorityID"]);

            data.priorityRequired = Convert.ToInt32(dr["PriorityRequired"]);
            data.accountGroup = dr["AccountGroup"].ToString();
            data.enableConector = Convert.ToInt32(dr["EnableConnector"]);
            data.woChargeId = Convert.ToInt32(dr["WOChargeID"]);
            data.woPickupReceiveChargeId = Convert.ToInt32(dr["WOPickupReceiveChargeID"]);

            if (!dr.IsNull("AccountReference"))
                data.accountReference = dr["AccountReference"].ToString();

            data.contactPinRequiredByDefault = Convert.ToInt32(dr["ContactPINRequiredByDefault"]);
        }

        internal void FillRow(core.be.RsAccount data, System.Data.DataRow dr)
        {
            dr["AccountID"] = data.accountId;
            dr["Level1AccountID"] = data.level1AccountId;
            dr["Level2AccountID"] = data.level2AccountId;
            dr["Level3AccountID"] = data.level3AccountId;
            dr["Level2ParentAccountID"] = data.level2ParentAccountId;
            dr["Level3ParentAccountID"] = data.level3ParentAccountId;
            dr["AccountCode"] = data.accountCode;
            dr["AccountName"] = data.accountName;

            if (data.updtDtime.Year != 1)
                dr["LeaseExpireDtime"] = data.leaseExpireDtime;

            if (data.updtDtime.Year != 1)
                dr["SetupDtime"] = data.setupDtime;

            dr["IsDeletable"] = data.isDeletable;
            dr["IsOpen"] = data.isOpen;
            dr["ForceAuthorization"] = data.forceAuthorization;
            dr["SalesmanCode"] = data.salesmanCode;
            dr["PONumber"] = data.poNumber;
            dr["InvoiceCycleID"] = data.invoiceCycleId;
            dr["IsDelinquent"] = data.isDelinquent;
            dr["ApplyPriorityToWO"] = data.applyPriorityToWo;
            dr["AccountingCustCode"] = data.accountingCustCode;
            dr["TallyInOutDaysContainer"] = data.tallyInOutDaysContainer;

            if (data.updtDtime.Year != 1)
                dr["TallyInOutDaysContainerDate"] = data.tallyInOutDaysContainerDate;

            dr["TallyInOutDaysFilefolder"] = data.tallyInOutDaysFilefolder;

            if (data.updtDtime.Year != 1)
                dr["TallyInOutDaysFilefolderDate"] = data.tallyInOutDaysFilefolderDate;

            dr["TallyInOutDaysTape"] = data.tallyInOutDaysTape;

            if (data.updtDtime.Year != 1)
                dr["TallyInOutDaysTapeDate"] = data.tallyInOutDaysTapeDate;

            dr["UpdtUserID"] = data.updtUserId;

            if (data.updtDtime.Year != 1)
                dr["UpdtDtime"] = data.updtDtime;

            dr["ContentValidation"] = data.contentValidation;
            dr["UseParentRetention"] = data.useParentRetention;
            dr["ApplyPriorityToItem"] = data.applyPriorityToItem;
            dr["ApplyLinkedPrioritiesToActions"] = data.applyLinkedPrioritiesToActions;
            dr["DefaultPriorityID"] = data.defaultPriorityId;
            dr["PriorityRequired"] = data.priorityRequired;
            dr["AccountGroup"] = data.accountGroup;
            dr["EnableConnector"] = data.enableConector;
            dr["WOChargeID"] = data.woChargeId;
            dr["WOPickupReceiveChargeID"] = data.woPickupReceiveChargeId;
            dr["AccountReference"] = data.accountReference;
            dr["ContactPINRequiredByDefault"] = data.contactPinRequiredByDefault;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public System.Data.DataTable GetDataTable(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataTable dt;
            dt = new System.Data.DataTable();

            dataAdapter.Fill(dt);

            return dt;
        }

        public List<core.be.RsAccount> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);
            List<core.be.RsAccount> vec = new List<be.RsAccount>();

            core.be.RsAccount data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new core.be.RsAccount();
                FillData(row, data);
                vec.Add(data);
            }
            return vec;
        }

        public List<core.be.RsAccount> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSACCOUNT order by AccountID", dbTrans);
        }

        public System.Data.DataTable GetLevel1Accounts(System.Data.Common.DbTransaction dbTrans)
        {
            return GetDataTable("select * from RSACCOUNT where AccountID = Level1AccountID order by AccountID", dbTrans);
        }

        public System.Data.DataTable GetLevel2Accounts(int accountLevel1, System.Data.Common.DbTransaction dbTrans)
        {
            return GetDataTable("select * from RSACCOUNT where AccountID = Level2AccountID and Level1AccountID = "
                + accountLevel1 + " order by AccountID", dbTrans);
        }

        public System.Data.DataTable GetLevel3Accounts(int accountLevel1, int accountLevel2, System.Data.Common.DbTransaction dbTrans)
        {
            return GetDataTable("select * from RSACCOUNT where AccountID = Level3AccountID and Level1AccountID = "
                + accountLevel1 + " and Level2AccountID = " + accountLevel2 + " order by AccountID", dbTrans);
        }
    }
}
