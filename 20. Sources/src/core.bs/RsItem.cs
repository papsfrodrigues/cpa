﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsItem
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsItem(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsItem data)
        {
            data.itemId = System.Convert.ToInt32(dr["ItemID"]);

            if (!dr.IsNull("LocationID"))
                data.locationId = System.Convert.ToInt32(dr["LocationID"]);

            if (!dr.IsNull("ContainerItemID"))
                data.containerItemId = System.Convert.ToInt32(dr["ContainerItemID"]);

            if (!dr.IsNull("ContainedInDtime"))
                data.containedInDtime = System.Convert.ToDateTime(dr["ContainedInDtime"]);

            data.accountId = System.Convert.ToInt32(dr["AccountID"]);
            data.baseObjectId = System.Convert.ToInt16(dr["BaseObjectID"]);
            data.objectId = System.Convert.ToInt16(dr["ObjectID"]);

            if (!dr.IsNull("ItemSecurityID"))
                data.itemSecurityId = System.Convert.ToInt16(dr["ItemSecurityID"]);

            if (!dr.IsNull("CategoryID"))
                data.categoryId = System.Convert.ToInt32(dr["CategoryID"]);

            if (!dr.IsNull("ContaineeObjectID"))
                data.containeeObjectID = System.Convert.ToInt16(dr["ContaineeObjectID"]);
            data.itemStatusID = System.Convert.ToByte(dr["ItemStatusID"]);
            data.itemCode = System.Convert.ToString(dr["ItemCode"]);

            if (!dr.IsNull("AlternateCode"))
                data.alternateCode = System.Convert.ToString(dr["AlternateCode"]);

            data.statusDtime = System.Convert.ToDateTime(dr["StatusDtime"]);
            data.addDtime = System.Convert.ToDateTime(dr["AddDtime"]);

            if (!dr.IsNull("DestroyDtime"))
                data.destroyDtime = System.Convert.ToDateTime(dr["DestroyDtime"]);

            data.permanentFlag = System.Convert.ToByte(dr["PermanentFlag"]);
            data.chargeForStorage = System.Convert.ToByte(dr["ChargeForStorage"]);
            data.containeeDefaultCharge = System.Convert.ToByte(dr["ContaineeDefaultCharge"]);

            if (!dr.IsNull("FromDtime"))
                data.fromDtime = System.Convert.ToDateTime(dr["FromDtime"]);

            if (!dr.IsNull("ToDtime"))
                data.toDtime = System.Convert.ToDateTime(dr["ToDtime"]);

            if (!dr.IsNull("SeqBegin"))
                data.seqBegin = System.Convert.ToString(dr["SeqBegin"]);

            if (!dr.IsNull("SeqEnd"))
                data.seqEnd = System.Convert.ToString(dr["SeqEnd"]);

            if (!dr.IsNull("UserField1"))
                data.userField1 = System.Convert.ToString(dr["UserField1"]);

            if (!dr.IsNull("UserField2"))
                data.userField2 = System.Convert.ToString(dr["UserField2"]);

            if (!dr.IsNull("UserField3"))
                data.userField3 = System.Convert.ToString(dr["UserField3"]);

            if (!dr.IsNull("UserField4"))
                data.userField4 = System.Convert.ToString(dr["UserField4"]);

            if (!dr.IsNull("ItemDesc"))
                data.itemDesc = System.Convert.ToString(dr["ItemDesc"]);

            if (!dr.IsNull("UserDtime"))
                data.userDtime = System.Convert.ToDateTime(dr["UserDtime"]);

            if (!dr.IsNull("ItemSetName"))
                data.itemSetName = System.Convert.ToString(dr["ItemSetName"]);

            data.accessCount = System.Convert.ToInt16(dr["AccessCount"]);
            data.updtUserID = System.Convert.ToInt16(dr["UpdtUserID"]);
            data.updtDtime = System.Convert.ToDateTime(dr["UpdtDtime"]);

            if (!dr.IsNull("RecordSeriesID"))
                data.recordSeriesID = System.Convert.ToInt32(dr["RecordSeriesID"]);

            if (!dr.IsNull("PendingAddDtime"))
                data.pendingAddDtime = System.Convert.ToDateTime(dr["PendingAddDtime"]);
        }

        internal void FillRow(core.be.RsItem data, System.Data.DataRow dr)
        {
            dr["ItemID"] = data.itemId;
            dr["LocationID"] = data.locationId;
            dr["ContainerItemID"] = data.containerItemId;

            if (data.containedInDtime.Year != 1)
                dr["ContainedInDtime"] = data.containedInDtime;

            dr["AccountID"] = data.accountId;
            dr["BaseObjectID"] = data.baseObjectId;
            dr["ObjectID"] = data.objectId;
            dr["ItemSecurityID"] = data.itemSecurityId;
            dr["CategoryID"] = data.categoryId;
            dr["ContaineeObjectID"] = data.containeeObjectID;
            dr["ItemStatusID"] = data.itemStatusID;
            dr["ItemCode"] = data.itemCode;
            dr["AlternateCode"] = data.alternateCode;

            if (data.statusDtime.Year != 1)
                dr["StatusDtime"] = data.statusDtime;

            if (data.addDtime.Year != 1)
                dr["AddDtime"] = data.addDtime;

            if (data.destroyDtime.Year != 1)
                dr["DestroyDtime"] = data.destroyDtime;

            dr["PermanentFlag"] = data.permanentFlag;
            dr["ChargeForStorage"] = data.chargeForStorage;
            dr["ContaineeDefaultCharge"] = data.containeeDefaultCharge;

            if (data.fromDtime.Year != 1)
                dr["FromDtime"] = data.fromDtime;

            if (data.toDtime.Year != 1)
                dr["ToDtime"] = data.toDtime;

            dr["SeqBegin"] = data.seqBegin;
            dr["SeqEnd"] = data.seqEnd;
            dr["UserField1"] = data.userField1;
            dr["UserField2"] = data.userField2;
            dr["UserField3"] = data.userField3;
            dr["UserField4"] = data.userField4;
            dr["ItemDesc"] = data.itemDesc;

            if (data.userDtime.Year != 1)
                dr["UserDtime"] = data.userDtime;

            dr["ItemSetName"] = data.itemSetName;
            dr["AccessCount"] = data.accessCount;
            dr["UpdtUserID"] = data.updtUserID;

            if (data.updtDtime.Year != 1)
                dr["UpdtDtime"] = data.updtDtime;

            dr["RecordSeriesID"] = data.recordSeriesID;

            if (data.pendingAddDtime.Year != 1)
                dr["PendingAddDtime"] = data.pendingAddDtime;
        }

        public System.Data.Common.DbDataAdapter CreateDataAdapter(System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = "select * from RSITEM where ItemID = 0";
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.Common.DbCommandBuilder commandBuilder;
            commandBuilder = this.dataProviderFactory.CreateCommandBuilder();
            commandBuilder.DataAdapter = dataAdapter;

            dataAdapter.InsertCommand = commandBuilder.GetInsertCommand();
            dataAdapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            dataAdapter.DeleteCommand = commandBuilder.GetDeleteCommand();

            return dataAdapter;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public System.Data.DataTable GetDataTable(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;
            dataCommand.CommandTimeout = 100000;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataTable dt;
            dt = new System.Data.DataTable();

            dataAdapter.Fill(dt);

            return dt;
        }

        public List<core.be.RsItem> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);
            List<core.be.RsItem> vec = new List<be.RsItem>();

            core.be.RsItem data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new be.RsItem();
                FillData(row, data);
                vec.Add(data);
            }
            return vec;
        }

        public List<core.be.RsItem> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSITEM order by ItemCode", dbTrans);
        }

        public System.Data.DataTable GetSomeList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetDataTable("select * from RSITEM, RSOBJECT, RSLOCATION WHERE ItemDesc = '' order by ItemCode", dbTrans);
        }

        public System.Data.DataTable GetList(int accountId, string description, System.Data.Common.DbTransaction dbTrans)
        {
            string itemTable = "RSITEM item";

            if (!String.IsNullOrEmpty(description))
            {
                itemTable = "(SELECT itemC.ItemID, itemC.ItemStatusID, itemC.AddDtime, itemC.UpdtDtime, itemC.DestroyDtime, "
                            + "itemC.ItemDesc, itemC.ItemCode, itemC.UpdtUserID, itemC.ObjectID, itemC.LocationID, itemC.AccountID "
                            + "FROM RSITEM itemC left join RSITEMCONTENT itemCnt "
                            + "on itemC.ItemID = itemCnt.ItemID "
                            + "where itemCnt.ItemContent like '%" + description + "%'"
                            + " union all "
                            + "SELECT itemD.ItemID, itemD.ItemStatusID, itemD.AddDtime, itemD.UpdtDtime, itemD.DestroyDtime, "
                            + "itemD.ItemDesc, itemD.ItemCode, itemD.UpdtUserID, itemD.ObjectID, itemD.LocationID, itemD.AccountID "
                            + "FROM RSITEM itemD left join RSITEMDESCRIPTION itemDescription "
                            + "on itemD.ItemID = itemDescription.ItemID "
                            + "where itemDescription.ItemDesc like '%" + description + "%'"
                            + " union all "
                            + "select newItem.ItemID, newItem.ItemStatusID, newItem.AddDtime, newItem.UpdtDtime, "
                            + "newItem.DestroyDtime, newItem.ItemDesc, newItem.ItemCode, newItem.UpdtUserID, "
                            + "newItem.ObjectID, newItem.LocationID, newItem.AccountID"
                            + " from RSITEM newItem where newItem.ItemDesc like '%" + description + "%'"
                            + ") as item";
            }

            string query = "select distinct item.ItemID, obj.ObjectDesc, item.ItemStatusID, item.AddDtime, item.UpdtDtime, "
                + "item.DestroyDtime, item.ItemDesc, item.ItemCode, item.UpdtUserID, loc.LocationDesc, item.AccountID "
                + "from " + itemTable + ", RSOBJECT obj, RSLOCATION loc where "
                + "item.ObjectID = obj.ObjectID and loc.LocationID = item.LocationID";
            
            if(accountId > 0)
            {
                query += " AND item.AccountID = '" + accountId + "'";
            }

            query += " order by item.ItemID";

            return GetDataTable(query, dbTrans);
        }
    }
}
