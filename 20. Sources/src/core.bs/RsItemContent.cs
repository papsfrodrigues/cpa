﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsItemContent
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsItemContent(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsItemContent data)
        {
            data.itemContentId = Convert.ToInt32(dr["ItemContentID"]);
            data.itemId = Convert.ToInt32(dr["ItemID"]);
            data.sequence = Convert.ToInt32(dr["Sequence"]);
            if (!dr.IsNull("ItemContent"))
                data.itemContent = dr["ItemContent"].ToString();
        }

        internal void FillRow(core.be.RsItemContent data, System.Data.DataRow dr)
        {
            dr["ItemContentID"] = data.itemContentId;
            dr["ItemID"] = data.itemId;
            dr["Sequence"] = data.sequence;
            dr["ItemContent"] = data.itemContent;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public System.Data.DataTable GetDataTable(int itemID, System.Data.Common.DbTransaction dbTrans)
        {
            string strCommand = "select * from RSITEMCONTENT where ItemID = '" + itemID + "'";

            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataTable dt;
            dt = new System.Data.DataTable();

            dataAdapter.Fill(dt);

            return dt;
        }

        public List<core.be.RsItemContent> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);
            List<core.be.RsItemContent> vec = new List<be.RsItemContent>();

            core.be.RsItemContent data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new core.be.RsItemContent();
                FillData(row, data);
                vec.Add(data);
            }
            return vec;
        }

        public List<core.be.RsItemContent> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSITEMCONTENT order by AccountID", dbTrans);
        }
    }
}
