﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsItemDescription
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsItemDescription(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsItemDescription data, bool areColumnsRenamed = false)
        {
            if (!dr.IsNull("ItemDescID"))
                data.itemDescId = System.Convert.ToInt32(dr["ItemDescID"]);

            if (!dr.IsNull("Sequence"))
                data.sequence = System.Convert.ToInt16(dr["Sequence"]);

            if (!areColumnsRenamed)
            {
                if (!dr.IsNull("ItemID"))
                    data.itemId = System.Convert.ToInt32(dr["ItemID"]);

                if (!dr.IsNull("ItemDesc"))
                    data.itemDesc = System.Convert.ToString(dr["ItemDesc"]);
            }
            else
            {
                if (!dr.IsNull("ItemIdDescription"))
                    data.itemId = System.Convert.ToInt32(dr["ItemIdDescription"]);

                if (!dr.IsNull("ItemDescription"))
                    data.itemDesc = System.Convert.ToString(dr["ItemDescription"]);
            }
        }

        internal void FillRow(core.be.RsItemDescription data, System.Data.DataRow dr, bool areColumnsRenamed = false)
        {
            dr["ItemDescID"] = data.itemDescId;
            dr["Sequence"] = data.sequence;

            if (!areColumnsRenamed)
            {
                dr["ItemID"] = data.itemId;
                dr["ItemDesc"] = data.itemDesc;
            }
            else
            {
                dr["ItemIdDescription"] = data.itemId;
                dr["ItemDescription"] = data.itemDesc;
            }
        }

        public System.Data.Common.DbDataAdapter CreateDataAdapter(System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = "select * from RSITEMDESCRIPTION where ItemDescID = 0";
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.Common.DbCommandBuilder commandBuilder;
            commandBuilder = this.dataProviderFactory.CreateCommandBuilder();
            commandBuilder.DataAdapter = dataAdapter;

            dataAdapter.InsertCommand = commandBuilder.GetInsertCommand();
            dataAdapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            dataAdapter.DeleteCommand = commandBuilder.GetDeleteCommand();

            return dataAdapter;
        }

        public System.Data.DataTable GetDataTable(int itemID, System.Data.Common.DbTransaction dbTrans)
        {
            string strCommand = "select * from RSITEMDESCRIPTION where ItemID = '" + itemID + "'";

            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataTable dt;
            dt = new System.Data.DataTable();

            dataAdapter.Fill(dt);

            return dt;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public List<core.be.RsItemDescription> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);

            List<core.be.RsItemDescription> vec = new List<be.RsItemDescription>();

            be.RsItemDescription data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new be.RsItemDescription();
                FillData(row, data);
                vec.Add(data);
            }

            return vec;
        }

        public List<core.be.RsItemDescription> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSITEMDESCRIPTION", dbTrans);
        }

        public List<core.be.RsItemDescription> GetList(int itemId, System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSITEMDESCRIPTION where ItemID = " + itemId, dbTrans);
        }
    }
}
