﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsLocation
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsLocation(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsLocation data)
        {
            data.locationId = Convert.ToInt32(dr["LocationID"]);
            data.objectId = Convert.ToInt32(dr["ObjectId"]);
            data.capacity = Convert.ToInt32(dr["Capacity"]);
            data.currentQuantity = Convert.ToInt32(dr["CurrentQuantity"]);
            data.accountId = Convert.ToInt32(dr["AccountID"]);
            data.building = dr["Building"].ToString();
            data.floor = dr["Floor"].ToString();
            data.room = dr["Room"].ToString();
            data.holdAreaId = Convert.ToInt32(dr["HoldAreaID"]);
            data.locationCode = dr["LocationCode"].ToString();
            data.locationDesc = dr["LocationDesc"].ToString();
            data.updtUserId = Convert.ToInt32(dr["UpdtUserID"]);
            data.updtDtime = Convert.ToDateTime(dr["UpdtDtime"]);
        }

        internal void FillRow(core.be.RsLocation data, System.Data.DataRow dr)
        {
            dr["LocationID"] = data.locationId;
            dr["ObjectId"] = data.objectId;
            dr["Capacity"] = data.capacity;
            dr["CurrentQuantity"] = data.currentQuantity;
            dr["AccountID"] = data.accountId;
            dr["Building"] = data.building;
            dr["Floor"] = data.floor;
            dr["Room"] = data.room;
            dr["HoldAreaID"] = data.holdAreaId;
            dr["LocationCode"] = data.locationCode;
            dr["LocationDesc"] = data.locationDesc;
            dr["UpdtUserID"] = data.updtUserId;
            dr["UpdtDtime"] = data.updtDtime;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public List<core.be.RsLocation> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);
            List<core.be.RsLocation> vec = new List<be.RsLocation>();

            core.be.RsLocation data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new core.be.RsLocation();
                FillData(row, data);
                vec.Add(data);
            }
            return vec;
        }

        public List<core.be.RsLocation> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSLOCATION order by AccountID", dbTrans);
        }
    }
}
