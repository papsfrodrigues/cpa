﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.bs
{
    public class RsObject
    {
        protected System.Data.Common.DbProviderFactory dataProviderFactory;
        protected System.Data.Common.DbConnection dataConnection;

        public RsObject(System.Data.Common.DbProviderFactory providerFactory, System.Data.Common.DbConnection dataConn)
        {
            this.dataProviderFactory = providerFactory;
            this.dataConnection = dataConn;
        }

        internal void FillData(System.Data.DataRow dr, core.be.RsObject data)
        {
            data.objectId = Convert.ToInt32(dr["ObjectID"]);
            data.parentObjectId = Convert.ToInt32(dr["ParentObjectID"]);
            data.objectCode = dr["ObjectCode"].ToString();
            data.objectDesc = dr["ObjectDesc"].ToString();
            data.enableChildren = Convert.ToInt32(dr["EnableChildren"]);
            data.isDeletable = Convert.ToInt32(dr["IsDeletable"]);
            data.objectTypeId = Convert.ToInt32(dr["ObjectTypeID"]);
            data.isPublic = Convert.ToInt32(dr["IsPublic"]);
            data.updtUserId = Convert.ToInt32(dr["UpdtUserID"]);
            data.updtDtime = Convert.ToDateTime(dr["UpdtDtime"]);
            data.unitVolume = Convert.ToDecimal(dr["UnitVolume"]);
        }

        internal void FillRow(core.be.RsObject data, System.Data.DataRow dr)
        {
            dr["ObjectID"] = data.objectId;
            dr["ParentObjectID"] = data.parentObjectId;
            dr["ObjectCode"] = data.objectCode;
            dr["ObjectDesc"] = data.objectDesc;
            dr["EnableChildren"] = data.enableChildren;
            dr["IsDeletable"] = data.isDeletable;
            dr["ObjectTypeID"] = data.objectTypeId;
            dr["IsPublic"] = data.isPublic;
            dr["UpdtUserID"] = data.updtUserId;
            dr["UpdtDtime"] = data.updtDtime;
            dr["UnitVolume"] = data.unitVolume;
        }

        public System.Data.DataSet GetDataSet(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.Common.DbCommand dataCommand;
            dataCommand = this.dataProviderFactory.CreateCommand();
            dataCommand.Connection = this.dataConnection;
            dataCommand.CommandText = strCommand;
            dataCommand.Transaction = dbTrans;

            System.Data.Common.DbDataAdapter dataAdapter;
            dataAdapter = this.dataProviderFactory.CreateDataAdapter();
            dataAdapter.SelectCommand = dataCommand;

            System.Data.DataSet ds;
            ds = new System.Data.DataSet();

            dataAdapter.Fill(ds);

            return ds;
        }

        public List<core.be.RsObject> GetList(string strCommand, System.Data.Common.DbTransaction dbTrans)
        {
            System.Data.DataSet ds = GetDataSet(strCommand, dbTrans);
            List<core.be.RsObject> vec = new List<be.RsObject>();

            core.be.RsObject data;

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                data = new core.be.RsObject();
                FillData(row, data);
                vec.Add(data);
            }
            return vec;
        }

        public List<core.be.RsObject> GetList(System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSOBJECT order by ObjectID", dbTrans);
        }

        public List<core.be.RsObject> GetListFromObjectId(int objId, System.Data.Common.DbTransaction dbTrans)
        {
            return GetList("select * from RSOBJECT where ObjectID = '" + objId + "'", dbTrans);
        }
    }
}
