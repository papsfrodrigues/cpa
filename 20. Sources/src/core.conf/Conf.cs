﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.conf
{
    public class Conf : Papiro.platform.conf.PlatConfiguration
    {
        // Database
        public string strDatabaseProvider;
        public string strDatabaseSchema;
        public string strDatabaseServerName;
        public string strDatabaseName;
        public string strDatabaseUserName;
        public string strDatabasePassword;

        public Conf()
        {
            Properties.Settings settConf;
            settConf = new Properties.Settings();

            this.strDatabaseProvider = settConf.database_provider;
            this.strDatabaseSchema = settConf.database_schema;
            this.strDatabaseServerName = settConf.database_instance;
            this.strDatabaseName = settConf.database_name;
            this.strDatabaseUserName = settConf.database_login_user;
            this.strDatabasePassword = settConf.database_login_password;
        }

        public string GetDataConnectionString()
        {
            string strDataConnection;

            strDataConnection = Papiro.platform.conf.AdoConf.BuildConnectionString(
                    this.strDatabaseProvider, this.strDatabaseServerName, this.strDatabaseName, this.strDatabaseUserName, this.strDatabasePassword);

            return strDataConnection;
        }

        public string GetDataProvider()
        {
            return this.strDatabaseProvider;
        }
    }
}
