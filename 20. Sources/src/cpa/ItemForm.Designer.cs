﻿namespace cpa
{
    partial class ItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemDescDgv = new Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable();
            this.itemContentDgv = new Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.itemDescDgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemContentDgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // itemDescDgv
            // 
            this.itemDescDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemDescDgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemDescDgv.Location = new System.Drawing.Point(0, 0);
            this.itemDescDgv.Name = "itemDescDgv";
            this.itemDescDgv.Size = new System.Drawing.Size(373, 443);
            this.itemDescDgv.TabIndex = 4;
            // 
            // itemContentDgv
            // 
            this.itemContentDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemContentDgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemContentDgv.Location = new System.Drawing.Point(0, 0);
            this.itemContentDgv.Name = "itemContentDgv";
            this.itemContentDgv.Size = new System.Drawing.Size(349, 443);
            this.itemContentDgv.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(23, 63);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.itemContentDgv);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.itemDescDgv);
            this.splitContainer1.Size = new System.Drawing.Size(726, 443);
            this.splitContainer1.SplitterDistance = 349;
            this.splitContainer1.TabIndex = 1;
            // 
            // ItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 529);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ItemForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Conteúdo Item";
            this.Load += new System.EventHandler(this.ItemForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemDescDgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemContentDgv)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable itemDescDgv;
        private Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable itemContentDgv;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}