﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cpa
{
    public partial class ItemForm : MetroFramework.Forms.MetroForm
    {
        int itemId;

        public ItemForm(int id)
        {
            this.itemId = id;

            InitializeComponent();
        }

        private void ItemForm_Load(object sender, EventArgs e)
        {
            LoadGrids();
        }

        private void LoadGrids()
        {
            //CONTENT GRID
            core.bs.RsItemContent contentBs = new core.bs.RsItemContent(Program.dataProviderFactory, Program.dataConnection);
            DataTable dtContent = contentBs.GetDataTable(itemId, null);

            string[] colsContent = { "ItemContentID", "ItemID", "Sequence", "ItemContent" };
            InitializeGrid(this.itemContentDgv, dtContent, colsContent, "ItemContentID");

            //DESCRIPTION GRID
            core.bs.RsItemDescription descriptionBs = new core.bs.RsItemDescription(Program.dataProviderFactory, Program.dataConnection);
            DataTable dtDescription = descriptionBs.GetDataTable(itemId, null);

            string[] colsDesc = { "ItemDescID", "ItemID", "Sequence", "ItemDesc" };
            InitializeGrid(this.itemDescDgv, dtDescription, colsDesc, "ItemDescID");
        }

        private void InitializeGrid(Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable grid, DataTable dtValues, string[] cols, string colKey)
        {
            grid.ClearView();
            grid.columnsConf = null;

            grid.columnsConf = new Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable.ColumnsConf();

            foreach(string col in cols)
                grid.columnsConf.Add(col, col, 80, false, true, "");

            grid.strKeyColumnName = colKey;

            grid.dt = dtValues;
            grid.Create();
            grid.LoadData();
        }

        private void FillGrid(Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable grid, DataTable dtValues)
        {
            grid.ClearView();
            grid.dt = dtValues;
            grid.LoadData();
        }

        public void UpdateGrids(int id)
        {
            this.itemId = id;

            //UPDATE CONTENT GRID
            core.bs.RsItemContent contentBs = new core.bs.RsItemContent(Program.dataProviderFactory, Program.dataConnection);
            DataTable dtContent = contentBs.GetDataTable(itemId, null);
            FillGrid(this.itemContentDgv, dtContent);

            //UPDATE DESCRIPTION GRID
            core.bs.RsItemDescription descriptionBs = new core.bs.RsItemDescription(Program.dataProviderFactory, Program.dataConnection);
            DataTable dtDescription = descriptionBs.GetDataTable(itemId, null);

            FillGrid(this.itemDescDgv, dtDescription);
        }
    }
}
