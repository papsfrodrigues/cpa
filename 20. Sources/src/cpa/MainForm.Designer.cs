﻿namespace cpa
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.accountLevel1 = new Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox();
            this.accountLevel2 = new Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox();
            this.accountLevel3 = new Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox();
            this.resultsDataGridView = new Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable();
            this.searchBtn = new System.Windows.Forms.Button();
            this.accountLbl = new System.Windows.Forms.Label();
            this.descLbl = new System.Windows.Forms.Label();
            this.descTxbx = new System.Windows.Forms.TextBox();
            this.statusStripMain = new Papiro.platform.us.winforms.controls.StatusStripMainWindow();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToCsvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // accountLevel1
            // 
            this.accountLevel1.AnchorClause = "";
            this.accountLevel1.ColID = "";
            this.accountLevel1.ColOrder = "";
            this.accountLevel1.ColText = "";
            this.accountLevel1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accountLevel1.Dt = null;
            this.accountLevel1.FormattingEnabled = true;
            this.accountLevel1.Location = new System.Drawing.Point(70, 93);
            this.accountLevel1.Name = "accountLevel1";
            this.accountLevel1.SelectedItemID = null;
            this.accountLevel1.SelectedItemText = null;
            this.accountLevel1.Size = new System.Drawing.Size(212, 21);
            this.accountLevel1.TabIndex = 0;
            this.accountLevel1.SelectedValueChanged += new System.EventHandler(this.accountLevel1_SelectedValueChanged);
            // 
            // accountLevel2
            // 
            this.accountLevel2.AnchorClause = "";
            this.accountLevel2.ColID = "";
            this.accountLevel2.ColOrder = "";
            this.accountLevel2.ColText = "";
            this.accountLevel2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accountLevel2.Dt = null;
            this.accountLevel2.FormattingEnabled = true;
            this.accountLevel2.Location = new System.Drawing.Point(288, 93);
            this.accountLevel2.Name = "accountLevel2";
            this.accountLevel2.SelectedItemID = null;
            this.accountLevel2.SelectedItemText = null;
            this.accountLevel2.Size = new System.Drawing.Size(208, 21);
            this.accountLevel2.TabIndex = 1;
            this.accountLevel2.SelectedValueChanged += new System.EventHandler(this.accountLevel2_SelectedValueChanged);
            // 
            // accountLevel3
            // 
            this.accountLevel3.AnchorClause = "";
            this.accountLevel3.ColID = "";
            this.accountLevel3.ColOrder = "";
            this.accountLevel3.ColText = "";
            this.accountLevel3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accountLevel3.Dt = null;
            this.accountLevel3.FormattingEnabled = true;
            this.accountLevel3.Location = new System.Drawing.Point(502, 93);
            this.accountLevel3.Name = "accountLevel3";
            this.accountLevel3.SelectedItemID = null;
            this.accountLevel3.SelectedItemText = null;
            this.accountLevel3.Size = new System.Drawing.Size(217, 21);
            this.accountLevel3.TabIndex = 2;
            // 
            // resultsDataGridView
            // 
            this.resultsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsDataGridView.Location = new System.Drawing.Point(20, 146);
            this.resultsDataGridView.Name = "resultsDataGridView";
            this.resultsDataGridView.Size = new System.Drawing.Size(810, 359);
            this.resultsDataGridView.TabIndex = 5;
            this.resultsDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.resultsDataGridView_CellDoubleClick);
            this.resultsDataGridView.SelectionChanged += new System.EventHandler(this.resultsDataGridView_SelectionChanged);
            // 
            // searchBtn
            // 
            this.searchBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchBtn.Location = new System.Drawing.Point(744, 112);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(83, 30);
            this.searchBtn.TabIndex = 4;
            this.searchBtn.Text = "PESQUISAR";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // accountLbl
            // 
            this.accountLbl.AutoSize = true;
            this.accountLbl.Location = new System.Drawing.Point(26, 97);
            this.accountLbl.Name = "accountLbl";
            this.accountLbl.Size = new System.Drawing.Size(38, 13);
            this.accountLbl.TabIndex = 11;
            this.accountLbl.Text = "Conta:";
            // 
            // descLbl
            // 
            this.descLbl.AutoSize = true;
            this.descLbl.Location = new System.Drawing.Point(25, 125);
            this.descLbl.Name = "descLbl";
            this.descLbl.Size = new System.Drawing.Size(58, 13);
            this.descLbl.TabIndex = 12;
            this.descLbl.Text = "Descrição:";
            // 
            // descTxbx
            // 
            this.descTxbx.Location = new System.Drawing.Point(90, 122);
            this.descTxbx.Name = "descTxbx";
            this.descTxbx.Size = new System.Drawing.Size(629, 20);
            this.descTxbx.TabIndex = 3;
            this.descTxbx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.descTxbx_KeyPress);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Location = new System.Drawing.Point(20, 508);
            this.statusStripMain.Message = "Ready";
            this.statusStripMain.MessageToolTip = null;
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.ShowItemToolTips = true;
            this.statusStripMain.Size = new System.Drawing.Size(810, 22);
            this.statusStripMain.TabIndex = 7;
            this.statusStripMain.Text = "statusStripMainWindow1";
            this.statusStripMain.Version = "3.10.941.1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(810, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportToCsvToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // exportToCsvToolStripMenuItem
            // 
            this.exportToCsvToolStripMenuItem.Name = "exportToCsvToolStripMenuItem";
            this.exportToCsvToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exportToCsvToolStripMenuItem.Text = "Export to csv...";
            this.exportToCsvToolStripMenuItem.Click += new System.EventHandler(this.exportToCsvToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 550);
            this.Controls.Add(this.resultsDataGridView);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.descTxbx);
            this.Controls.Add(this.descLbl);
            this.Controls.Add(this.accountLbl);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.accountLevel3);
            this.Controls.Add(this.accountLevel2);
            this.Controls.Add(this.accountLevel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "CPA";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox accountLevel1;
        private Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox accountLevel2;
        private Papiro.platform.us.winforms.datacontrols.AliasDataTableComboBox accountLevel3;
        private Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable resultsDataGridView;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label accountLbl;
        private System.Windows.Forms.Label descLbl;
        private System.Windows.Forms.TextBox descTxbx;
        private Papiro.platform.us.winforms.controls.StatusStripMainWindow statusStripMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exportToCsvToolStripMenuItem;
    }
}

