﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cpa
{
    public partial class MainForm : MetroFramework.Forms.MetroForm
    {
        core.bs.RsAccount bsAccount;

        public MainForm()
        {
            InitializeComponent();
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Form fc = Application.OpenForms["ItemForm"];
                if (fc != null)
                {
                    fc.Close();
                }

                int accountId = -1;
                if (!String.IsNullOrEmpty(this.accountLevel3.SelectedItemID))
                    accountId = Convert.ToInt32(this.accountLevel3.SelectedItemID);
                else if (!String.IsNullOrEmpty(this.accountLevel2.SelectedItemID))
                    accountId = Convert.ToInt32(this.accountLevel2.SelectedItemID);
                else if (!String.IsNullOrEmpty(this.accountLevel1.SelectedItemID))
                    accountId = Convert.ToInt32(this.accountLevel1.SelectedItemID);

                string searchDesc = "";

                if (!String.IsNullOrEmpty(this.descTxbx.Text))
                    searchDesc = this.descTxbx.Text;

                core.bs.RsItem bsItem = new core.bs.RsItem(Program.dataProviderFactory, Program.dataConnection);
                System.Data.DataTable dataResult = bsItem.GetList(accountId, searchDesc, null);

                if (dataResult.Rows.Count > 0)
                {
                    this.resultsDataGridView.dt = dataResult;
                    this.resultsDataGridView.ClearView();
                    this.resultsDataGridView.LoadData();

                    this.statusStripMain.Items[2].Text = dataResult.Rows.Count + " registos";
                }
                else
                {
                    MessageBox.Show("Sem resultados!","INFORMAÇÂO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //
            //load account level 1 dropdown

            bsAccount = new core.bs.RsAccount(Program.dataProviderFactory, Program.dataConnection);
            DataTable accountsLvl1 = bsAccount.GetLevel1Accounts(null);

            
            this.accountLevel1.ColID = "AccountID";
            this.accountLevel1.ColText = "AccountName";
            this.accountLevel1.ColOrder = "AccountID";
            this.accountLevel1.Dt = accountsLvl1;
            this.accountLevel1.AddNull();
            this.accountLevel1.Load();

            //
            //configuration datagrid

            this.resultsDataGridView.columnsConf = null;

            this.resultsDataGridView.columnsConf = new Papiro.platform.us.winforms.datacontrols.DataGridViewDataTable.ColumnsConf();
            this.resultsDataGridView.columnsConf.Add("ItemID", "ItemID", 70, false, true, "");
            this.resultsDataGridView.columnsConf.Add("ObjectDesc", "ObjectDesc", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("ItemStatusID", "ItemStatusID", 80, false, true, "");
            this.resultsDataGridView.columnsConf.Add("AddDtime", "AddDtime", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("UpdtDtime", "UpdtDtime", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("DestroyDtime", "DestroyDtime", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("ItemDesc", "ItemDesc", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("ItemCode", "ItemCode", 70, false, true, "");
            this.resultsDataGridView.columnsConf.Add("UpdtUserID", "UpdtUserID", 70, false, true, "");
            this.resultsDataGridView.columnsConf.Add("LocationDesc", "LocationDesc", 100, false, true, "");
            this.resultsDataGridView.columnsConf.Add("AccountID", "AccountID", 70, false, true, "");
            this.resultsDataGridView.strKeyColumnName = "ItemID";

            core.bs.RsItem bsItem = new core.bs.RsItem(Program.dataProviderFactory, Program.dataConnection);
            this.resultsDataGridView.dt = bsItem.GetSomeList(null);
            this.resultsDataGridView.Create();
            this.resultsDataGridView.LoadData();
        }

        private void accountLevel1_SelectedValueChanged(object sender, EventArgs e)
        {
            this.accountLevel2.Clear();
            this.accountLevel3.Clear();

            if (!String.IsNullOrEmpty(this.accountLevel1.SelectedItemID))
            {
                int id = Convert.ToInt32(this.accountLevel1.SelectedItemID);
                DataTable accounts2 = bsAccount.GetLevel2Accounts(id, null);

                this.accountLevel2.ColID = "AccountID";
                this.accountLevel2.ColText = "AccountName";
                this.accountLevel2.ColOrder = "AccountID";
                this.accountLevel2.Dt = accounts2;
                this.accountLevel2.AddNull();
                this.accountLevel2.Load();
            }
        }

        private void accountLevel2_SelectedValueChanged(object sender, EventArgs e)
        {
            this.accountLevel3.Clear();

            if (!String.IsNullOrEmpty(this.accountLevel2.SelectedItemID))
            {
                int id1 = Convert.ToInt32(this.accountLevel1.SelectedItemID);
                int id2 = Convert.ToInt32(this.accountLevel2.SelectedItemID);
                DataTable accounts3 = bsAccount.GetLevel3Accounts(id1, id2, null);

                this.accountLevel3.ColID = "AccountID";
                this.accountLevel3.ColText = "AccountName";
                this.accountLevel3.ColOrder = "AccountID";
                this.accountLevel3.Dt = accounts3;
                this.accountLevel3.AddNull();
                this.accountLevel3.Load();
            }
        }

        private void resultsDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int itemid = 0;
            if (int.TryParse(this.resultsDataGridView.Rows[e.RowIndex].Cells["ItemID"].Value.ToString(), out itemid))
            {
                Form fc = Application.OpenForms["ItemForm"];
                if (fc != null)
                {
                    //if item form opened update grids values
                    ItemForm itemForm = (ItemForm)fc;
                    int id = 0;
                    if (int.TryParse(this.resultsDataGridView.Rows[this.resultsDataGridView.SelectedCells[0].RowIndex].Cells["ItemID"].Value.ToString(), out id))
                    {
                        itemForm.UpdateGrids(id);
                    }
                }
                else
                {
                    ItemForm itemForm = new ItemForm(itemid);
                    itemForm.StartPosition = FormStartPosition.CenterParent;
                    itemForm.Show();
                }
            }
        }

        private void resultsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["ItemForm"];
            if(fc != null)
            {
                //if item form opened update grids values
                ItemForm itemForm = (ItemForm)fc;
                int id = 0;
                if (int.TryParse(this.resultsDataGridView.Rows[this.resultsDataGridView.SelectedCells[0].RowIndex].Cells["ItemID"].Value.ToString(), out id))
                {
                    itemForm.UpdateGrids(id);
                }
            }
        }

        private void descTxbx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                Search();
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Papiro.platform.us.winforms.controls.DataGridViewConfDialog dlgConf;
                dlgConf = new Papiro.platform.us.winforms.controls.DataGridViewConfDialog();

                dlgConf.t = Program.conf.GetType();
                dlgConf.obInstance = Program.conf;
                dlgConf.keyValueConfigurationCollection = null;

                dlgConf.StartPosition = FormStartPosition.CenterParent;
                if (dlgConf.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    Program.conf.Save();
                }
            }
            catch (System.Exception ex)
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportToCsvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.resultsDataGridView.Rows.Count > 1)
                {
                    Ookii.Dialogs.VistaSaveFileDialog saveDlg = new Ookii.Dialogs.VistaSaveFileDialog();
                    saveDlg.Filter = "CSV Files (*.csv)|*.csv";
                    if (saveDlg.ShowDialog() == DialogResult.OK)
                    {
                        this.Cursor = Cursors.WaitCursor;

                        string pathName = saveDlg.FileName + ".csv";

                        StringBuilder dataToFile = new StringBuilder();

                        dataToFile.AppendLine("ItemID;ObjectDesc;ItemStatusID;AddDtime;UpdtDtime;DestroyDtime;ItemDesc;"
                            + "ItemCode;UpdtUserID;LocationDesc;AccountID;ItemDescDescription");

                        foreach (DataGridViewRow row in this.resultsDataGridView.Rows)
                        {
                            dataToFile.Append(WriteRowLine(row));
                        }

                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(pathName, false, System.Text.Encoding.UTF8))
                        {
                            writer.Write(dataToFile);
                        }

                        this.Cursor = Cursors.Default;
                        MessageBox.Show("Exportação efetuada com sucesso", "SUCESSO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Deve fazer pesquisa antes!","AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string WriteRowLine(DataGridViewRow row)
        {
            if (row.Cells["ItemID"].Value != null)
            {

                string rowLines = "";

                string commonLine = row.Cells["ItemID"].Value.ToString() + ";" + row.Cells["ObjectDesc"].Value.ToString() + ";"
                    + row.Cells["ItemStatusID"].Value.ToString() + ";" + row.Cells["AddDtime"].Value.ToString() + ";"
                    + row.Cells["UpdtDtime"].Value.ToString() + ";" + row.Cells["DestroyDtime"].Value.ToString() + ";"
                    + row.Cells["ItemDesc"].Value.ToString() + ";" + row.Cells["ItemCode"].Value.ToString() + ";"
                    + row.Cells["UpdtUserID"].Value.ToString() + ";" + row.Cells["LocationDesc"].Value.ToString()
                    + ";" + row.Cells["AccountID"].Value.ToString();

                core.bs.RsItemDescription bsDesc = new core.bs.RsItemDescription(Program.dataProviderFactory, Program.dataConnection);

                int id = -1;
                if (int.TryParse(row.Cells["ItemID"].Value.ToString(), out id))
                {
                    DataTable dt = bsDesc.GetDataTable(id, null);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow r in dt.Rows)
                        {
                            rowLines += commonLine + ";" + r["ItemDesc"].ToString() + "\r\n";
                        }
                    }
                    else
                    {
                        rowLines += commonLine + ";\r\n";
                    }
                }

                return rowLines;
            }
            else
            {
                return "";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
