﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cpa
{
    static class Program
    {
        public static core.conf.Conf conf;

        public static System.Data.Common.DbProviderFactory dataProviderFactory;
        public static System.Data.Common.DbConnection dataConnection;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            conf = null;
            dataProviderFactory = null;
            dataConnection = null;

            try
            {
                conf = new core.conf.Conf();
                conf.OpenUsingAssemblyCodeBase("core.conf.dll");
                if (conf.TryLoadFieldString("strDatabaseProvider") != string.Empty)
                {
                    conf.Load();
                }

                OpenDataConnection();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (dataConnection != null)
            {
                dataConnection.Close();
                dataConnection = null;
            }
        }

        //
        public static bool OpenDataConnection()
        {
            bool bOk = false;
            string strDataConnection = "";

            try
            {
                dataProviderFactory = System.Data.Common.DbProviderFactories.GetFactory(conf.GetDataProvider());

                //Provider=SQLOLEDB.1;
                strDataConnection = conf.GetDataConnectionString();

                if (strDataConnection != "")
                {
                    dataConnection = dataProviderFactory.CreateConnection();
                    dataConnection.ConnectionString = strDataConnection;
                    dataConnection.Open();

                    bOk = true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("DataConnection Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.Data.OleDb.OleDbException oleDbEx)
            {
                dataConnection = null;
                System.Windows.Forms.MessageBox.Show(oleDbEx.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (System.Exception ex)
            {
                dataConnection = null;
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bOk;
        }

        public static void CloseDataConnection()
        {
            if (dataConnection != null)
            {
                dataConnection.Close();
                dataConnection = null;
            }
        }
        //
    }
}
